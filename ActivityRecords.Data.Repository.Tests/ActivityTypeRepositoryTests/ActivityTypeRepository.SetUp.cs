﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Repositories;
using NUnit.Framework;
using System.Linq;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class ActivityTypeRepositoryTests : TestBase
    {
        private ActivityTypeRepository _activityTypeRepository;
        private UserRepository _userRepository;

        [OneTimeSetUp]
        public new void SetUp()
        {
            base.SetUp();
            _activityTypeRepository = new ActivityTypeRepository(_context, _mapper);
            _userRepository = new UserRepository(_context, _mapper);
        }

        private bool ActivityTypesAreEqual(ActivityType activityType1, ActivityType activityType2)
        {
            return activityType1.Name == activityType2.Name
                && activityType1.UserId == activityType2.UserId
                && (activityType1.DetailNames != null && activityType2.DetailNames != null) 
                    ? Enumerable.SequenceEqual(activityType1.DetailNames, activityType2.DetailNames) : true;
        }
    }
}
