﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;
using System.Linq;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class ActivityTypeRepositoryTests
    {
        [Test]
        public void ShouldDeleteExistingActivityType()
        {
            // arrange
            ActivityType inputActivityType = new ActivityType
            {
                Name = "TestActivity1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            User user = new User
            {
                Login = "testlogin1"
            };
            int userId;

            // act
            userId = _userRepository.Create(user).Id;
            inputActivityType.UserId = userId;
            ActivityType resultActivityType = _activityTypeRepository.Create(inputActivityType);
            int activityTypeCount = 0;
            TestDelegate deleteActivityType = () =>
            {
                _activityTypeRepository.Delete(resultActivityType.Id);
                activityTypeCount = _context.ActivityTypes.Count();
            };
            ActivityType queryActivityType = null;
            TestDelegate getActivityType = () => queryActivityType = _activityTypeRepository.GetById(resultActivityType.Id);
            
            // assert
            Assert.DoesNotThrow(deleteActivityType);
            Assert.Throws(typeof(RepositoryException), getActivityType);
            Assert.AreEqual(activityTypeCount, 0);
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnDeletingNonExistingActivityType()
        {
            // arrange
            int id = -1;

            // act
            ActivityType queryActivityType = null;
            TestDelegate getActivityType = () => queryActivityType = _activityTypeRepository.GetById(id);
            int activityTypeCount = 0;
            TestDelegate deleteActivityType = () =>
            {
                _activityTypeRepository.Delete(id);
                activityTypeCount = _context.ActivityTypes.Count();
            };

            // assert
            Assert.Throws(typeof(RepositoryException), getActivityType);
            Assert.Throws(typeof(RepositoryException), deleteActivityType);
            Assert.AreEqual(activityTypeCount, 0);
        }
    }
}
