﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;
using System;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class ActivityRepositoryTests
    {
        [Test]
        public void ShouldReturnActivityOnCreateWithCorrectData()
        {
            // arrange 
            User user = new User { Login = "testlogin1" };
            ActivityType activityType = new ActivityType
            {
                Name = "TestActivityTypeName",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            Activity inputActivity = new Activity
            {
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "TestValue1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "TestValue2" }
                },
                Date = DateTime.Now
            };

            // act
            inputActivity.UserId = _userRepository.Create(user).Id;
            activityType.UserId = inputActivity.UserId;
            inputActivity.ActivityTypeId = _activityTypeRepository.Create(activityType).Id;
            Activity resultActivity = null;
            Activity queryActivity = null;
            TestDelegate createActivity = () => resultActivity = _activityRepository.Create(inputActivity);
            TestDelegate getActivity = () => queryActivity = _activityRepository.GetById(resultActivity.Id);

            // assert
            Assert.DoesNotThrow(createActivity);
            Assert.DoesNotThrow(getActivity);
            Assert.IsTrue(ActivitiesAreEqual(inputActivity, resultActivity));
            Assert.IsTrue(ActivitiesAreEqual(resultActivity, queryActivity));
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnCreateWithNonExistingUserAndActivityType()
        {
            // arrange
            Activity inputActivity = new Activity
            {
                ActivityTypeId = -1,
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "TestValue1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "TestValue2" }
                },
                Date = DateTime.Now
            };

            // act
            inputActivity.ActivityTypeId = -1;
            inputActivity.UserId = -1;
            Activity resultActivity = null;
            TestDelegate createActivity = () => resultActivity = _activityRepository.Create(inputActivity);

            // assert
            Assert.That(createActivity, Throws.TypeOf(typeof(RepositoryException)).And.Message.EqualTo("Specified user not found"));
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnCreateWithNonExistingActivityType()
        {
            // arrange 
            User user = new User { Login = "testlogin1" };
            int activityTypeId = -1;
            Activity inputActivity = new Activity
            {
                ActivityTypeId = -1,
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "TestValue1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "TestValue2" }
                },
                Date = DateTime.Now
            };

            // act
            inputActivity.UserId = _userRepository.Create(user).Id;
            inputActivity.ActivityTypeId = activityTypeId;
            Activity resultActivity = null;
            TestDelegate createActivity = () => resultActivity = _activityRepository.Create(inputActivity);

            // assert
            Assert.That(createActivity, Throws.TypeOf(typeof(RepositoryException)).And.Message.EqualTo("Specified activity type not found"));
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnCreateWithNonBelongingActivityType()
        {
            // arrange 
            User differentUser = new User
            {
                Login = "ownerlogin1"
            };
            User selectedUser = new User
            {
                Login = "testlogin2"
            };
            ActivityType differentUsersActivityType = new ActivityType
            {
                Name = "TestActivityType1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            Activity failedActivity = new Activity
            {
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "1" }
                }
            };

            // act
            differentUsersActivityType.UserId = _userRepository.Create(differentUser).Id;
            int activityId = _activityTypeRepository.Create(differentUsersActivityType).Id;
            failedActivity.UserId = _userRepository.Create(selectedUser).Id;
            failedActivity.ActivityTypeId = activityId;
            TestDelegate createActivity = () => _activityRepository.Create(failedActivity);

            // assert
            Assert.That(createActivity, Throws
                .TypeOf(typeof(RepositoryException))
                .And.Message.EqualTo("Specified user doesn't have specified activity type"));
        }

        [Test]
        public void ShouldReturnActivityOnCreateWithMinimumRequiredData()
        {
            // arrange 
            User user = new User { Login = "testlogin1" };
            ActivityType activityType = new ActivityType
            {
                Name = "TestActivityTypeName",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            Activity inputActivity = new Activity
            {
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "TestValue1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "TestValue2" }
                }
            };

            // act
            inputActivity.UserId = _userRepository.Create(user).Id;
            activityType.UserId = inputActivity.UserId;
            inputActivity.ActivityTypeId = _activityTypeRepository.Create(activityType).Id;
            Activity resultActivity = null;
            Activity queryActivity = null;
            TestDelegate createActivity = () => resultActivity = _activityRepository.Create(inputActivity);
            TestDelegate getActivity = () => queryActivity = _activityRepository.GetById(resultActivity.Id);

            // assert
            Assert.DoesNotThrow(createActivity);
            Assert.DoesNotThrow(getActivity);
            Assert.IsTrue(ActivitiesAreEqual(inputActivity, resultActivity));
            Assert.IsTrue(ActivitiesAreEqual(resultActivity, queryActivity));
        }
    }
}
