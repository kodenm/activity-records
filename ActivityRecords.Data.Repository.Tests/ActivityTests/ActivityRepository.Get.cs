﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;
using System;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class ActivityRepositoryTests
    {
        [Test]
        public void ShouldReturnUserOnGetOfExistingActivity()
        {
            // arrange 
            User user = new User { Login = "testlogin1" };
            ActivityType activityType = new ActivityType
            {
                Name = "TestActivityTypeName",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            Activity activity = new Activity
            {
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "TestValue1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "TestValue2" }
                },
                Date = DateTime.Now
            };

            // act
            activity.UserId = _userRepository.Create(user).Id;
            activityType.UserId = activity.UserId;
            activity.ActivityTypeId = _activityTypeRepository.Create(activityType).Id;
            Activity resultActivity = null;
            Activity queryActivity = null;
            TestDelegate createActivity = () => resultActivity = _activityRepository.Create(activity);
            TestDelegate getActivity = () => queryActivity = _activityRepository.GetById(resultActivity.Id);

            // assert
            Assert.DoesNotThrow(createActivity);
            Assert.DoesNotThrow(getActivity);
            Assert.IsTrue(ActivitiesAreEqual(activity, resultActivity));
            Assert.IsTrue(ActivitiesAreEqual(resultActivity, queryActivity));
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnGetOfNonExistingActivity()
        {
            // arrange
            int id = -1;

            // act
            TestDelegate getActivity = () => _activityRepository.GetById(id);

            // assert
            Assert.Throws(typeof(RepositoryException), getActivity);
        }
    }
}
