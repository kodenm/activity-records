﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class ActivityRepositoryTests
    {
        [Test]
        public void ShouldDeleteExistingActivity()
        {
            // arrange
            User user = new User
            {
                Login = "testlogin1"
            };
            ActivityType activityType = new ActivityType
            {
                Name = "TestActivityType1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            Activity activity = new Activity
            {
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "1" }
                }
            };

            // act
            activityType.UserId = _userRepository.Create(user).Id;
            activity.UserId = activityType.UserId;
            activity.ActivityTypeId = _activityTypeRepository.Create(activityType).Id;
            TestDelegate createActivity = () => activity = _activityRepository.Create(activity);
            TestDelegate deleteActivity = () => _activityRepository.Delete(activity.Id);

            // assert
            Assert.DoesNotThrow(createActivity);
            Assert.DoesNotThrow(deleteActivity);
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnDeleteOfNonExistingUser()
        {
            // arrange
            int id = 1;

            // act
            TestDelegate deleteActivity = () => _activityRepository.Delete(id);

            // assert
            Assert.Throws(typeof(RepositoryException), deleteActivity);
        }
    }
}
