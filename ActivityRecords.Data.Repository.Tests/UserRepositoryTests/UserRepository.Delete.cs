﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;
using System.Linq;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class UserRepositoryTests
    {
        [Test]
        public void ShouldDeleteExistingUser()
        {
            // arrange
            User creationUser = new User
            {
                Login = "testlogin1"
            };
            int id;

            // act
            id = _repository.Create(creationUser).Id;
            User resultUser = null;
            int userCount = 0;
            TestDelegate deleteUser = () =>
            {
                _repository.Delete(id);
            };
            TestDelegate obtainDeletedUser = () =>
            {
                resultUser = _repository.GetById(id);
                userCount = _context.Users.Count();
            };

            // assert
            Assert.DoesNotThrow(deleteUser);
            Assert.Throws(typeof(RepositoryException), obtainDeletedUser);
            Assert.IsNull(resultUser);
            Assert.That(userCount, Is.EqualTo(0));
        }

        [Test]
        public void ShouldDoNothingOnNonExistingUser()
        {
            // assert
            int id = -1;

            // act
            TestDelegate deleteUser = () => _repository.Delete(id);
            int userCount = _context.Users.Count();

            // assert
            Assert.Throws(typeof(RepositoryException), deleteUser);
            Assert.AreEqual(userCount, 0);
        }
    }
}
