﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class UserRepositoryTests
    {
        [Test]
        public void ShouldThrowRepositoryExceptionOnNonExistingUsersId()
        {
            // arrange
            int id = -1;

            // act
            User resultUser = null;
            TestDelegate getUser = () => { resultUser = _repository.GetById(id); };

            // assert
            Assert.Throws(typeof(RepositoryException), getUser);
            Assert.IsNull(resultUser);
        }

        [Test]
        public void ShouldReturnExistingUser()
        {
            // arrange
            User inputUser = new User
            {
                Login = "testlogin1"
            };

            // act
            int id = _repository.Create(inputUser).Id;
            User resultUser = null;
            TestDelegate getUser = () => { resultUser = _repository.GetById(id); };


            // assert
            Assert.DoesNotThrow(getUser);
            Assert.IsTrue(UsersAreEqual(resultUser, inputUser));
        }
    }
}
