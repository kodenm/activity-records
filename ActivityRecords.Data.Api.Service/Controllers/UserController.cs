﻿using Microsoft.AspNetCore.Mvc;
using ActivityRecords.Data.Api.Model.User;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Interfaces;
using AutoMapper;
using ActivityRecords.Data.Repository.Model.Exceptions;

namespace ActivityRecords.Data.Api.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private IUserRepository _repository;
        private IMapper _mapper;

        public UserController(IUserRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            User user;
            try
            {
                user = _repository.GetById(id);
            }
            catch (RepositoryException e)
            {
                return NotFound(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoUserModel>(user));
        }

        [HttpPost]
        public IActionResult Create(CreationUserModel user)
        {
            User resultUser;
            try
            {
                resultUser = _repository.Create(_mapper.Map<User>(user));
            }
            catch (RepositoryException e)
            {
                return BadRequest(e.Message);
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoUserModel>(resultUser));
        }

        [HttpPut("{userId}")]
        public IActionResult Update(int userId, UpdateUserModel user)
        {
            User resultUser; 
            try
            {
                resultUser = _repository.Update(userId, _mapper.Map<User>(user));
            }
            catch (RepositoryException e)
            {
                return BadRequest(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoUserModel>(resultUser));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
            }
            catch (RepositoryException e)
            {
                return NotFound(e.Message);
            }
            return Ok();
        }
    }
}
