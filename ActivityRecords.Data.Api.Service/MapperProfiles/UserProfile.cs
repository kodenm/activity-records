﻿using ActivityRecords.Data.Api.Model.User;
using ActivityRecords.Data.Repository.Model.Entities;
using AutoMapper;
using System;

namespace ActivityRecords.Data.Api.Service.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, InfoUserModel>()
                .ForMember(d => d.Id, opts => opts.MapFrom(s => s.Id))
                .ForMember(d => d.Login, opts => opts.MapFrom(s => s.Login))
                .ForMember(d => d.RegistrationDateTime, opts => opts.MapFrom(s => s.RegistrationDateTime))
                .ForMember(d => d.BirthDate, opts => opts.MapFrom(s => s.BirthDate))
                .ForMember(d => d.Height, opts => opts.MapFrom(s => s.Height))
                .ForMember(d => d.Weight, opts => opts.MapFrom(s => s.Weight));
            CreateMap<CreationUserModel, User>()
                .ForMember(d => d.Id, opts => opts.MapFrom(s => s.Id))
                .ForMember(d => d.Login, opts => opts.MapFrom(s => s.Login))
                .ForMember(d => d.BirthDate, opts => opts.MapFrom(s => s.BirthDate))
                .ForMember(d => d.RegistrationDateTime, opts => opts.MapFrom(s => DateTime.Now))
                .ForMember(d => d.Height, opts => opts.MapFrom(s => s.Height))
                .ForMember(d => d.Weight, opts => opts.MapFrom(s => s.Weight));
            CreateMap<UpdateUserModel, User>()
                .ForMember(d => d.Login, opts =>
                {
                    opts.PreCondition(s => s.Login != null);
                    opts.MapFrom(s => s.Login);
                })
                .ForMember(d => d.BirthDate, opts => opts.MapFrom(s => s.BirthDate))
                .ForMember(d => d.RegistrationDateTime, opts => opts.Ignore())
                .ForMember(d => d.Height, opts => opts.MapFrom(s => s.Height))
                .ForMember(d => d.Weight, opts => opts.MapFrom(s => s.Weight));
        }
    }
}
