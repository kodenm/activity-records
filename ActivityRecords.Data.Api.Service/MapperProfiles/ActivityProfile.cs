﻿using ActivityRecords.Data.Api.Model.Activity;
using ActivityRecords.Data.Api.Model.ActivityDetail;
using ActivityRecords.Data.Repository.Model.Entities;
using AutoMapper;
using System;
using System.Linq;

namespace ActivityRecords.Data.Api.Service.MapperProfiles
{
    public class ActivityProfile : Profile
    {
        public ActivityProfile()
        {
            CreateMap<Activity, InfoActivityModel>()
                .ForMember(d => d.Id, opts => opts.MapFrom(s => s.Id))
                .ForMember(d => d.ActivityTypeId, opts => opts.MapFrom(s => s.ActivityTypeId))
                .ForMember(d => d.UserId, opts => opts.MapFrom(s => s.UserId))
                .ForMember(d => d.DateTime, opts => opts.MapFrom(s => s.Date))
                .ForMember(d => d.Details, options =>
                {
                    options.PreCondition(s => s.Details != null);
                    options.MapFrom(s => s.Details.Select(d => new ActivityDetailModel { Name = d.Name, Value = d.Value }));
                });
            CreateMap<UpdateActivityModel, Activity>()
                .ForMember(d => d.Details, options =>
                {
                    options.PreCondition(s => s.Details != null);
                    options.MapFrom(s => s.Details.Select(d => new ActivityDetail { Name = d.Name, Value = d.Value }));
                });
            CreateMap<CreationActivityModel, Activity>()
                .ForMember(d => d.ActivityTypeId, opts => opts.MapFrom(s => s.ActivityTypeId))
                .ForMember(d => d.UserId, opts => opts.MapFrom(s => s.UserId))
                .ForMember(d => d.Date, opts => opts.MapFrom(s => s.DateTime))
                .ForMember(d => d.Details, options =>
                {
                    options.PreCondition(s => s.Details != null);
                    options.MapFrom(s => s.Details.Select(d => new ActivityDetail { Name = d.Name, Value = d.Value }));
                });
        }
    }
}
