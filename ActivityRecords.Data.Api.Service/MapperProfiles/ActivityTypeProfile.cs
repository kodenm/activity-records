﻿using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Repository.Model.Entities;
using AutoMapper;

namespace ActivityRecords.Data.Api.Service.MapperProfiles
{
    public class ActivityTypeProfile : Profile
    {
        public ActivityTypeProfile()
        {
            CreateMap<ActivityType, InfoActivityTypeModel>()
                .ForMember(d => d.Id, opts => opts.MapFrom(s => s.Id))
                .ForMember(d => d.Name, opts => opts.MapFrom(s => s.Name))
                .ForMember(d => d.UserId, opts => opts.MapFrom(s => s.UserId))
                .ForMember(d => d.DetailNames, opts => opts.MapFrom(s => s.DetailNames));
            CreateMap<CreationActivityTypeModel, ActivityType>()
                .ForMember(d => d.Name, opts => opts.MapFrom(s => s.Name))
                .ForMember(d => d.UserId, opts => opts.MapFrom(s => s.UserId))
                .ForMember(d => d.DetailNames, opts => opts.MapFrom(s => s.DetailNames));
            CreateMap<UpdateActivityTypeModel, ActivityType>()
                .ForMember(d => d.Name, opts => opts.MapFrom(s => s.Name))
                .ForMember(d => d.DetailNames, opts => opts.MapFrom(s => s.DetailNames));
        }
    }
}
