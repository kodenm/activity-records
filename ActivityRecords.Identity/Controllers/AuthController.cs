﻿using ActivityRecords.Identity.Model;
using ActivityRecords.Identity.Model.Entities;
using ActivityRecords.Identity.Model.Exceptions;
using ActivityRecords.Identity.Model.Tools;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;

namespace ActivityRecords.Identity.Api.Controllers
{
    [EnableCors]
    [ApiController]
    public class AuthController : Controller
    {
        private IIdentityRepository _repository;
        private IOptions<AuthOptions> _authOptions;

        public AuthController(IIdentityRepository repository, IOptions<AuthOptions> authOptions)
        {
            _repository = repository;
            _authOptions = authOptions;
        }

        [HttpPost("signup")]
        public IActionResult Register([FromBody] Credentials credentials)
        {
            try
            {
                return Ok(new { AccessToken = JwtGenerator.Generate(_repository.Create(credentials), _authOptions) });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] Credentials credentials)
        {
            try
            {
                return Ok(new { AccessToken = JwtGenerator.Generate(_repository.Authenticate(credentials), _authOptions) });
            }
            catch (InvalidCredentialsException e)
            {
                return Unauthorized(e.Message);
            }
        }

        [HttpPut("update/{id}")]
        public IActionResult Update(int id, [FromBody] Credentials credentials)
        {
            try
            {
                _repository.Update(id, credentials);
                return Ok();
            }
            catch (RepositoryException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return Ok();
            }
            catch (RepositoryException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
