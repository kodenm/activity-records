﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ActivityRecords.Client.Common
{
    public abstract class ClientBase
    {
        protected HttpClient _client;
        public ClientBase(Uri serverUri)
        {
            _client = new HttpClient();
            _client.BaseAddress = serverUri;
        }
        protected async Task<HttpResponseMessage> PostAsyncInternal(string requestUri, object obj)
        {
            var content = new StringContent(JsonConvert.SerializeObject(obj));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await _client.PostAsync(requestUri, content);
        }

        protected async Task<HttpResponseMessage> PutAsyncInternal(string requestUri, object obj)
        {
            var content = new StringContent(JsonConvert.SerializeObject(obj));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return await _client.PutAsync(requestUri, content);
        }

        protected async Task<T> ResponseToObject<T>(HttpResponseMessage response)
        {
            var body = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(body);
        }
    }
}
