﻿using ActivityRecords.Identity.Api.Model.Entities;
using System.Threading.Tasks;

namespace ActivityRecords.Identity.Api.Model.Interfaces
{
    public interface IIdentityClient
    {
        Task<int> RegisterAsync(Credentials credentials);
        Task DeleteAsync(int id);
        Task UpdateAsync(int id, Credentials credentials);
    }
}
