﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityRecords.Identity.Api.Model.Entities
{
    public class Credentials
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
