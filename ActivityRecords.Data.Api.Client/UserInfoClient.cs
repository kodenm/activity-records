﻿using ActivityRecords.Client.Common;
using ActivityRecords.Data.Api.Model.Interfaces;
using ActivityRecords.Data.Api.Model.User;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ActivityRecords.Data.Api.Client
{
    public class UserInfoClient : ClientBase, IUserInfoClient
    {
        public UserInfoClient(Uri serverUri) : base(serverUri) { }
        public async Task<InfoUserModel> CreateAsync(CreationUserModel creationUser)
        {
            var response = await PostAsyncInternal("user", creationUser);
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoUserModel>(response);
        }

        public async Task DeleteAsync(int id)
        {
            var response = await _client.DeleteAsync($"user/{id}");
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<InfoUserModel> GetByIdAsync(int id)
        {
            var response = await _client.GetAsync($"user/{id}");
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoUserModel>(response);
        }

        public async Task<InfoUserModel> UpdateAsync(int id, UpdateUserModel updateUser)
        {
            var response = await PutAsyncInternal($"user/{id}", updateUser);
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoUserModel>(response);
        }
    }
}
