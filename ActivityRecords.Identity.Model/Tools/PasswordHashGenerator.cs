﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ActivityRecords.Identity.Tools
{
    public static class PasswordHashGenerator
    {
        public static string Generate(string password)
        {
            return string.Concat(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(password)).Select(b => b.ToString("x2")));
        }
    }
}
