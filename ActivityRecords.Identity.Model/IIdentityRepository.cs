﻿using ActivityRecords.Identity.Model.Entities;

namespace ActivityRecords.Identity.Model
{
    public interface IIdentityRepository
    {
        User Create(Credentials credentials);
        User Authenticate(Credentials credentials);
        void Delete(int id);
        void Update(int id, Credentials credentials);
    }
}
