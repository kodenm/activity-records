﻿using System;

namespace ActivityRecords.Identity.Model.Exceptions
{
    public class InvalidCredentialsException : Exception
    {
        public InvalidCredentialsException(string message) : base(message)
        {

        }
    }
}
