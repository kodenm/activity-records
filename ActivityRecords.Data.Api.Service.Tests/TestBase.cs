﻿using ActivityRecords.Data.Api.Service.MapperProfiles;
using ActivityRecords.Data.Repository.Model.Interfaces;
using AutoMapper;
using Moq;

namespace ActivityRecords.Data.Api.Service.Tests
{
    public class TestBase
    {
        protected IMapper _mapper;
        protected Mock<IUserRepository> _userRepositoryMock;
        protected Mock<IActivityRepository> _activityRepositoryMock;
        protected Mock<IActivityTypeRepository> _activityTypeRepositoryMock;

        public TestBase()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(typeof(ActivityProfile));
                cfg.AddProfile(typeof(UserProfile));
                cfg.AddProfile(typeof(ActivityTypeProfile));
            });
            _mapper = mapperConfiguration.CreateMapper();
        }
    }
}
