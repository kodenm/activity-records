﻿using ActivityRecords.Data.Api.Model.Activity;
using ActivityRecords.Data.Api.Model.ActivityDetail;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityControllerTests
{
    public partial class ActivityControllerTests
    {
        [Test]
        public void ShouldReturnActivityInfoOnCreateWithCorrectData()
        {
            // arrange
            CreationActivityModel inputActivity = new CreationActivityModel
            {
                ActivityTypeId = 1,
                DateTime = DateTime.Now,
                UserId = 1,
                Details = new[]
                {
                    new ActivityDetailModel { Name = "testdetailname1", Value = "1" },
                    new ActivityDetailModel { Name = "testdetailname2", Value = "1" }
                }
            };
            Activity resultActivity = new Activity
            {
                Id = 1,
                ActivityTypeId = 1,
                UserId = 1,
                Date = inputActivity.DateTime,
                Details = inputActivity.Details.Select(d => new ActivityDetail { ActivityId = 1, Name = d.Name, Value = d.Value })
            };
            _activityRepositoryMock.Setup(arm => arm.Create(It.IsAny<Activity>()))
                .Returns(resultActivity);

            // act
            var returnedActivity = (_activityController.Post(inputActivity) as JsonResult)?.Value as InfoActivityModel;

            // assert
            Assert.IsNotNull(returnedActivity);
            AssertActivitiesAreEqual(resultActivity, returnedActivity);
        }  
        
        [Test]
        public void ShouldReturnBadRequestOnCreateWhenRepositoryExceptionThrown()
        {
            // arrange
            CreationActivityModel inputActivity = new CreationActivityModel
            {
                ActivityTypeId = 1,
                UserId = 1
            };
            _activityRepositoryMock.Setup(arm => arm.Create(It.IsAny<Activity>()))
                .Throws(new RepositoryException("repository error"));

            // act
            var contentResult = _activityController.Post(inputActivity) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "repository error");
        }

        [Test]
        public void ShouldReturnBadRequestOnCreateWhenValidationExceptionThrown()
        {
            // arrange
            CreationActivityModel inputActivity = new CreationActivityModel
            {
                ActivityTypeId = 1,
                UserId = 1
            };
            _activityRepositoryMock.Setup(arm => arm.Create(It.IsAny<Activity>()))
                .Throws(new ValidationException("validation error"));

            // act
            var contentResult = _activityController.Post(inputActivity) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "validation error");
        }
    }
}
