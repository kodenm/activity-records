﻿using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityControllerTests
{
    public partial class ActivityControllerTests
    {
        [Test]
        public void ShouldReturnOkOnDeleteOfActivityWithCorrectData()
        {
            // arrange
            int activityId = 1;
            _activityRepositoryMock.Setup(arm => arm.Delete(It.IsAny<int>()));

            // act
            var contentResult = _activityController.Delete(activityId) as OkResult;

            // assert
            Assert.IsNotNull(contentResult);
        }

        [Test]
        public void ShouldReturnNotFoundOnDeleteWhenRepositoryExceptionThrown()
        {
            // arrange
            int activityId = 1;
            _activityRepositoryMock.Setup(arm => arm.Delete(It.IsAny<int>()))
                .Throws(new RepositoryException("not found"));

            // act
            var contentResult = _activityController.Delete(activityId) as NotFoundObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "not found");
        }
    }
}
