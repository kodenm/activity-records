﻿using ActivityRecords.Data.Api.Model.Activity;
using ActivityRecords.Data.Api.Model.ActivityDetail;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Linq;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityControllerTests
{
    public partial class ActivityControllerTests
    {
        [Test]
        public void ShouldReturnBadRequestOnUpdateWhenRepositoryExceptionThrown()
        {
            // arrange
            int activityId = -1;
            UpdateActivityModel inputActivity = new UpdateActivityModel
            {
                Details = new[]
                {
                    new ActivityDetailModel { Name = "test1", Value = "1" },
                    new ActivityDetailModel { Name = "test2", Value = "1" }
                }
            };
            _activityRepositoryMock.Setup(arm => arm.Update(It.IsAny<int>(), It.IsAny<Activity>()))
                .Throws(new RepositoryException("inner repository exception"));

            // act
            var contentResult = _activityController.Put(activityId, inputActivity) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "inner repository exception");
        }

        [Test]
        public void ShouldReturnInfoActivityOnUpdateWithCorrectData()
        {
            // arrange
            int activityId = 1;
            UpdateActivityModel inputActivity = new UpdateActivityModel
            {
                Details = new[]
                {
                    new ActivityDetailModel { Name = "testdetailname1", Value = "1" },
                    new ActivityDetailModel { Name = "testdetailname2", Value = "1" }
                }
            };
            Activity resultActivity = new Activity
            {
                Id = activityId,
                ActivityTypeId = 1,
                UserId = 1,
                Date = DateTime.Now,
                Details = inputActivity.Details.Select(d => new ActivityDetail { ActivityId = 1, Name = d.Name, Value = d.Value })
            };
            _activityRepositoryMock.Setup(arm => arm.Update(It.IsAny<int>(), It.IsAny<Activity>()))
                .Returns(resultActivity);

            // act
            var returnedActivity = (_activityController.Put(activityId, inputActivity) as JsonResult)?.Value as InfoActivityModel;

            // assert
            Assert.IsNotNull(returnedActivity);
            AssertActivitiesAreEqual(resultActivity, returnedActivity);
        }
    }
}
