﻿using ActivityRecords.Data.Api.Model.Activity;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityControllerTests
{
    public partial class ActivityControllerTests
    {
        [Test]
        public void ShouldReturnActivityInfoOnGetWithCorrectData()
        {
            // arrange
            int activityId = 1;
            Activity inputActivity = new Activity
            {
                Id = 1,
                ActivityTypeId = 1,
                UserId = 1,
                Date = DateTime.Now,
                Details = new[]
                {
                    new ActivityDetail { Name = "test1", Value = "1" },
                    new ActivityDetail { Name = "test2", Value = "1" }
                }
            };
            _activityRepositoryMock.Setup(arm => arm.GetById(It.IsAny<int>()))
                .Returns(inputActivity);

            // act
            var returnedActivity = (_activityController.Get(activityId) as JsonResult)?.Value as InfoActivityModel;

            // assert
            Assert.IsNotNull(returnedActivity);
            AssertActivitiesAreEqual(inputActivity, returnedActivity);
        }

        [Test]
        public void ShouldReturnNotFoundOnGetWhenRepositoryExceptionThrown()
        {
            // arrange
            int activityId = -1;
            _activityRepositoryMock.Setup(arm => arm.GetById(It.IsAny<int>()))
                .Throws(new RepositoryException("not found"));

            // act
            var contentResult = _activityController.Get(activityId) as NotFoundObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "not found");
        }

        [Test]
        public void ShouldReturnIEnumerableActivitiesOnGetByUserIdWithCorrectData()
        {
            // arrange
            int userId = 1;
            List<Activity> inputList = new List<Activity>
            {
                new Activity
                {
                    Id = 1,
                    ActivityTypeId = 1,
                    UserId = userId,
                    Date = DateTime.Now,
                    Details = new[]
                    {
                        new ActivityDetail { ActivityId = 1, Name = "test1", Value = "1" },
                        new ActivityDetail { ActivityId = 1, Name = "test2", Value = "1" }
                    }
                },
                new Activity
                {
                    Id = 2,
                    ActivityTypeId = 1,
                    UserId = userId,
                    Date = DateTime.Now,
                    Details = new[]
                    {
                        new ActivityDetail { ActivityId = 1, Name = "test1", Value = "1" },
                        new ActivityDetail { ActivityId = 1, Name = "test2", Value = "1" }
                    }
                }
            };
            _activityRepositoryMock.Setup(arm => arm.GetByUserId(It.IsAny<int>()))
                .Returns(inputList);

            // act
            var returnedList = (_activityController.GetByUserId(userId) as JsonResult)?.Value as IEnumerable<InfoActivityModel>;

            // assert
            Assert.IsNotNull(returnedList);
            Assert.AreEqual(returnedList.Count(), inputList.Count());
            // IEnumerable<Activity> and IEnumerable<InfoActivityModel> must be compared
        }
    }
}
