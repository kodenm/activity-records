﻿using ActivityRecords.Data.Api.Model.Activity;
using ActivityRecords.Data.Api.Model.ActivityDetail;
using ActivityRecords.Data.Api.Service.Controllers;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityControllerTests
{
    [TestFixture]
    public partial class ActivityControllerTests : TestBase
    {
        private ActivityController _activityController;
        [SetUp]
        public void TestControllerSetUp()
        {
            _activityRepositoryMock = new Mock<IActivityRepository>();
            _activityController = new ActivityController(_activityRepositoryMock.Object, _mapper);
        }

        private void AssertActivitiesAreEqual(Activity inputActivity, InfoActivityModel outputActivity)
        {
            Assert.AreEqual(inputActivity.ActivityTypeId, outputActivity.ActivityTypeId);
            Assert.AreEqual(inputActivity.UserId, outputActivity.UserId);
            Assert.AreEqual(inputActivity.Date, outputActivity.DateTime);
            Assert.IsTrue(ActivityDetailsAreEqual(inputActivity.Details, outputActivity.Details));
        }

        private bool ActivityDetailsAreEqual(IEnumerable<ActivityDetail> inputActivityDetails, IEnumerable<ActivityDetailModel> outputActivityDetail)
        {
            return Enumerable.SequenceEqual(
                inputActivityDetails.Select(d => new { Name = d.Name, Value = d.Value }),
                outputActivityDetail.Select(d => new { Name = d.Name, Value = d.Value }));
        }

        private class ActivityComparer : IEqualityComparer
        {
            public new bool Equals(object x, object y)
            {
                if (x is Activity activity1 && y is InfoActivityModel activity2)
                {
                    return activity1.ActivityTypeId == activity2.ActivityTypeId
                        && activity1.UserId == activity2.UserId
                        && activity1.Date == activity2.DateTime
                        && Enumerable.SequenceEqual(
                            activity1.Details.Select(d => new { Name = d.Name, Value = d.Value }),
                            activity2.Details.Select(d => new { Name = d.Name, Value = d.Value }));
                }
                else if (x is InfoActivityModel activity3 && y is Activity activity4)
                {
                    return activity3.ActivityTypeId == activity4.ActivityTypeId
                        && activity3.UserId == activity4.UserId
                        && activity3.DateTime == activity4.Date
                        && Enumerable.SequenceEqual(
                            activity3.Details.Select(d => new { Name = d.Name, Value = d.Value }),
                            activity4.Details.Select(d => new { Name = d.Name, Value = d.Value }));
                }
                return false;
            }

            public int GetHashCode(object obj)
            {
                throw new System.NotImplementedException();
            }
        }
    }

}
