﻿using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Api.Service.Controllers;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Interfaces;
using Moq;
using NUnit.Framework;
using System.Linq;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityTypeControllerTests
{
    [TestFixture]
    public partial class ActivityTypeControllerTests : TestBase
    {
        private ActivityTypeController _activityTypeController;

        [SetUp]
        public void TestControllerSetUp()
        {
            _activityTypeRepositoryMock = new Mock<IActivityTypeRepository>();
            _activityTypeController = new ActivityTypeController(_activityTypeRepositoryMock.Object, _mapper);
        }

        public void AssertActivityTypesAreEqual(ActivityType inputActivityType, InfoActivityTypeModel outputActivityType)
        {
            Assert.AreEqual(inputActivityType.Id, outputActivityType.Id);
            Assert.AreEqual(inputActivityType.Name, outputActivityType.Name);
            Assert.AreEqual(inputActivityType.UserId, outputActivityType.UserId);
            if (inputActivityType.DetailNames != null && inputActivityType.DetailNames != null) 
            {
                Assert.IsTrue(Enumerable.SequenceEqual(inputActivityType.DetailNames, outputActivityType.DetailNames));
            }
            else if (inputActivityType.DetailNames != null ^ inputActivityType.DetailNames != null)
            {
                Assert.Fail("detail names are not the same");
            }
        }
    }
}
