﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityTypeControllerTests
{
    public partial class ActivityTypeControllerTests
    {
        [Test]
        public void ShouldReturnOkOnDeleteWithCorrectData()
        {
            // arrange
            int activityTypeId = 1;
            _activityTypeRepositoryMock.Setup(atrm => atrm.GetById(It.IsAny<int>()))
                .Returns(new ActivityType
                {
                    Id = 1,
                    Name = "TestActivityName1",
                });
            _activityTypeRepositoryMock.Setup(atrm => atrm.Delete(It.IsAny<int>()));

            // act
            var contentResult = _activityTypeController.Delete(activityTypeId) as OkResult;

            // assert
            Assert.IsNotNull(contentResult);
        }

        [Test]
        public void ShouldReturnNotFoundOnDeleteWhenRepositoryExceptionThrown()
        {
            // arrange
            int activityTypeId = -1;
            _activityTypeRepositoryMock.Setup(atrm => atrm.Delete(It.IsAny<int>()))
                .Throws(new RepositoryException("error on delete"));

            // act
            var contentResult = _activityTypeController.Delete(activityTypeId) as NotFoundObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "error on delete");
        }
    }
}
