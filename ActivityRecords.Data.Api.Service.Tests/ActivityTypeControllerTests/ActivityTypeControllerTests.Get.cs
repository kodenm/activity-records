﻿using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityTypeControllerTests
{
    public partial class ActivityTypeControllerTests
    {
        [Test]
        public void ShouldReturnActivityTypeInfoOnGetOfExistingActivityType()
        {
            // arrange
            int existingId = 1;
            ActivityType activityType = new ActivityType
            {
                Id = existingId,
                Name = "TestActivityName1",
                UserId = 1,
                DetailNames = new[]
                    {
                        "TestDetail1",
                        "TestDetail2"
                    }
            };
            _activityTypeRepositoryMock.Setup(atrm => atrm.GetById(It.IsAny<int>()))
                .Returns(activityType);

            // act
            var returnedUser = (_activityTypeController.Get(existingId) as JsonResult)?.Value as InfoActivityTypeModel;

            // assert
            Assert.IsNotNull(returnedUser);
            AssertActivityTypesAreEqual(activityType, returnedUser);
        }

        [Test]
        public void ShouldReturnNotFoundOnGetOfWhenRepositoryExceptionThrown()
        {
            // arrange
            int nonExistingId = -1;
            _activityTypeRepositoryMock.Setup(atrm => atrm.GetById(It.IsAny<int>()))
                .Throws(new RepositoryException("activity type not found"));

            // act
            var contentResult = _activityTypeController.Get(nonExistingId) as NotFoundObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "activity type not found");
        }

        [Test]
        public void ShouldReturnIEnumerableOnGetWithCorrectData()
        {
            // arrange
            int userId = 1;
            List<ActivityType> inputActivityTypes = new List<ActivityType>
            {
                new ActivityType
                {
                    Id = userId,
                    UserId = 1,
                    Name = "testactivityname1"
                },
                new ActivityType
                {
                    Id = 2,
                    UserId = userId,
                    Name = "testactivityname2"
                },
            };
            _activityTypeRepositoryMock.Setup(atrm => atrm.GetByUserId(It.IsAny<int>()))
                .Returns(inputActivityTypes);

            // act
            var returnedActivityTypes = (_activityTypeController.GetByUserId(userId) as JsonResult)?.Value as IEnumerable<InfoActivityTypeModel>;

            // assert
            Assert.IsNotNull(returnedActivityTypes);
            Assert.AreEqual(inputActivityTypes.Count(), returnedActivityTypes.Count());
        }

        [Test]
        public void ShouldReturnBadRequestOnGetOfIEnumerableWhenRepositoryExceptionThrown()
        {
            // arrange
            _activityTypeRepositoryMock.Setup(atrm => atrm.GetByUserId(It.IsAny<int>()))
                .Throws(new RepositoryException("repository exception in repository"));
            int userId = -1;

            // act
            var contentResult = _activityTypeController.GetByUserId(userId) as NotFoundObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "repository exception in repository");
        }
    }
}
