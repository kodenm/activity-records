﻿using ActivityRecords.Data.Api.Model.User;
using ActivityRecords.Data.Repository.Model.Exceptions;
using ActivityRecords.Data.Repository.Model.Entities;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;

namespace ActivityRecords.Data.Api.Service.Tests.UserControllerTests
{
    public partial class UserControllerTests
    {
        [Test]
        public void ShouldReturnUserInfoOnUpdateWithCorrectData()
        {
            // arrange
            int existingUserId = 1;
            UpdateUserModel updateUser = new UpdateUserModel
            {
                Login = "updatedlogin1",
                BirthDate = DateTime.Now,
                Weight = 2,
                Height = 2
            };
            User updatedUser = new User
            {
                Id = existingUserId,
                Login = updateUser.Login,
                RegistrationDateTime = DateTime.Now,
                BirthDate = updateUser.BirthDate,
                Weight = 2,
                Height = 2
            };
            _userRepositoryMock.Setup(urm => urm.Update(It.IsAny<int>(), It.IsAny<User>()))
                .Returns(updatedUser);

            // act
            InfoUserModel returnedUser = (_userController.Update(existingUserId, updateUser) as JsonResult)?.Value as InfoUserModel;

            // assert
            Assert.IsNotNull(returnedUser);
            UsersAreEqual(updatedUser, returnedUser);
        }

        [Test]
        public void ShouldReturnBadRequestOnUpdateWhenRepositoryExceptionThrown()
        {
            int nonExistingUserId = -1;
            UpdateUserModel updateUser = new UpdateUserModel
            {
                Login = "updatedlogin1",
                BirthDate = DateTime.Now,
                Weight = 2,
                Height = 2
            };
            _userRepositoryMock.Setup(urm => urm.Update(It.IsAny<int>(), It.IsAny<User>()))
                .Throws(new RepositoryException("no user with this id"));

            // act
            var resultObject = _userController.Update(nonExistingUserId, updateUser) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(resultObject);
            Assert.AreEqual(resultObject.Value, "no user with this id");
        }
    }
}
