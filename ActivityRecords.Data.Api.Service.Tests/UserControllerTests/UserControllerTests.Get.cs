﻿using ActivityRecords.Data.Api.Model.User;
using ActivityRecords.Data.Repository.Model.Exceptions;
using ActivityRecords.Data.Repository.Model.Entities;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;

namespace ActivityRecords.Data.Api.Service.Tests.UserControllerTests
{
    public partial class UserControllerTests
    {
        [Test]
        public void ShouldReturnBadRequestOnGetWhenRepositoryExceptionThrown()
        {
            // arrange
            _userRepositoryMock.Setup(urm => urm.GetById(It.IsAny<int>()))
                .Throws(new RepositoryException("User with specified id not found"));
            int nonExistingUserId = -1;

            // act
            IActionResult actionResult = _userController.Get(nonExistingUserId);
            var contentResult = actionResult as NotFoundObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "User with specified id not found");
        }

        [Test]
        public void ShouldReturnUserInfoOnGetWithCorrectData()
        {
            // arrange
            int existingUserId = 1;
            User expectedUser = new User {
                Id = existingUserId,
                Login = "testlogin1",
                RegistrationDateTime = DateTime.Now,
                BirthDate = DateTime.Now,
                Weight = 1,
                Height = 1
            };

            _userRepositoryMock.Setup(urm => urm.GetById(It.IsAny<int>()))
                .Returns(expectedUser);

            // act
            IActionResult actionResult = _userController.Get(existingUserId);
            var returnedUser = (actionResult as JsonResult)?.Value as InfoUserModel;

            // assert
            Assert.IsNotNull(returnedUser);
            UsersAreEqual(expectedUser, returnedUser);
        }
    }
}
