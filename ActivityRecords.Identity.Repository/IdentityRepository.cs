﻿using ActivityRecords.Identity.Model;
using ActivityRecords.Identity.Model.Entities;
using ActivityRecords.Identity.Model.Exceptions;
using ActivityRecords.Identity.Tools;
using System.Linq;

namespace ActivityRecords.Identity.Repository
{
    public class IdentityRepository : IIdentityRepository
    {
        public IdentityContext _context;

        public IdentityRepository(IdentityContext context)
        {
            _context = context;
        }

        public User Authenticate(Credentials credentials)
        {
            var user = _context.Users.SingleOrDefault(u => u.Login == credentials.Login && u.PasswordHash == PasswordHashGenerator.Generate(credentials.Password));
            if (user == null)
            {
                throw new InvalidCredentialsException("Credentials are invalid");
            }
            return user;
        }

        public void Delete(int id)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new RepositoryException("User not found");
            }
            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public User Create(Credentials credentials)
        {
            if (_context.Users.SingleOrDefault(u => u.Login == credentials.Login) != null)
            {
                throw new RepositoryException("Login is taken");
            }
            var user = _context.Users.Add(new User { Login = credentials.Login, PasswordHash = PasswordHashGenerator.Generate(credentials.Password) }).Entity;
            _context.SaveChanges();
            return user;
        }

        public void Update(int id, Credentials credentials)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new RepositoryException("User not found");
            }
            if (_context.Users.FirstOrDefault(u => u.Id != id && u.Login == credentials.Login) != null)
            {
                throw new RepositoryException("Login is taken");
            }
            user.Login = string.IsNullOrEmpty(credentials.Login) ? user.Login : credentials.Login;
            user.PasswordHash = string.IsNullOrEmpty(credentials.Password) ? user.PasswordHash : PasswordHashGenerator.Generate(credentials.Password);
            _context.SaveChanges();
        }
    }
}
