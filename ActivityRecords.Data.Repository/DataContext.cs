﻿using ActivityRecords.Data.Repository.Configurations;
using ActivityRecords.Data.Repository.EFModels;
using Microsoft.EntityFrameworkCore;


namespace ActivityRecords.Data.Repository
{
    public class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<ActivityDetail> ActivityDetails { get; set; }
        public DbSet<ActivityType> ActivityTypes { get; set; }
        public DbSet<ActivityTypeDetail> ActivityTypeDetails { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration())
                .ApplyConfiguration(new ActivityDetailEntityTypeConfiguration())
                .ApplyConfiguration(new ActivityEntityTypeConfiguration())
                .ApplyConfiguration(new ActivityTypeEntityTypeConfiguration())
                .ApplyConfiguration(new ActivityTypeDetailEntityTypeConfiguration());
        }
    }
}
