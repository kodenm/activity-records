﻿using System;
using System.Collections.Generic;

namespace ActivityRecords.Data.Repository.EFModels
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public DateTime RegistrationDateTime { get; set; }
        public DateTime? BirthDate { get; set; }
        public float? Weight { get; set; }
        public int? Height { get; set; }
        public IEnumerable<Activity> Activities { get; set; }
        public IEnumerable<ActivityType> ActivityTypes { get; set; }
    }
}
