﻿using ActivityRecords.Data.Repository.EFModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ActivityRecords.Data.Repository.Configurations
{
    public class ActivityDetailEntityTypeConfiguration : IEntityTypeConfiguration<ActivityDetail>
    {
        public void Configure(EntityTypeBuilder<ActivityDetail> builder)
        {
            builder.ToTable("ActivityDetail");
            builder.HasKey(activityDetail => new { activityDetail.ActivityId, activityDetail.Name });
            builder.Property(ad => ad.Name).HasMaxLength(50).IsRequired();
            builder.Property(ad => ad.Value).HasMaxLength(50);
        }
    }
}
