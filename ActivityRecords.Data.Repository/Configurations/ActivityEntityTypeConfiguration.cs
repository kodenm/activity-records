﻿using ActivityRecords.Data.Repository.EFModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ActivityRecords.Data.Repository.Configurations
{
    public class ActivityEntityTypeConfiguration : IEntityTypeConfiguration<Activity>
    {
        public void Configure(EntityTypeBuilder<Activity> builder)
        {
            builder.ToTable("Activity");
            builder.HasKey(activity => activity.Id);
            builder.HasMany(a => a.ActivityDetails).WithOne(ad => ad.Activity).HasForeignKey(ad => ad.ActivityId);      
            builder.Property(a => a.Date).IsRequired();
        }
    }
}
