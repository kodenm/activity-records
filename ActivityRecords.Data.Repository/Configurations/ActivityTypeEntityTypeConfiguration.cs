﻿using ActivityRecords.Data.Repository.EFModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ActivityRecords.Data.Repository.Configurations
{
    public class ActivityTypeEntityTypeConfiguration : IEntityTypeConfiguration<ActivityType>
    {
        public void Configure(EntityTypeBuilder<ActivityType> builder)
        {
            builder.ToTable("ActivityType");
            builder.HasKey(at => at.Id);
            builder.HasIndex(at => new { at.UserId, at.Name }).IsUnique();
            builder.Property(at => at.Name).HasMaxLength(50).IsRequired();
            builder.HasMany(at => at.Activities).WithOne(a => a.ActivityType).HasForeignKey(at => at.ActivityTypeId);
            builder.HasMany(at => at.ActivityTypeDetails).WithOne(atd => atd.ActivityType)
                .HasForeignKey(atd => atd.ActivityTypeId);
        }
    }
}
