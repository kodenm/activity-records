﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ActivityRecords.Data.Repository.Migrations
{
    public partial class RemovedDurationpropertyfromActivityentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Duration",
                table: "Activity");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<TimeSpan>(
                name: "Duration",
                table: "Activity",
                type: "time",
                nullable: true);
        }
    }
}
