﻿using AutoMapper;
using System;
using EFUser = ActivityRecords.Data.Repository.EFModels.User;
using ModelUser = ActivityRecords.Data.Repository.Model.Entities.User;

namespace ActivityRecords.Data.Repository.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<EFUser, ModelUser>()
                .ForMember(d => d.Id, memberOptions => memberOptions.MapFrom(s => s.Id))
                .ForMember(d => d.Login, memberOptions => memberOptions.Condition(s => s.Login != null))
                .ForMember(d => d.RegistrationDateTime, memberOptions => memberOptions.MapFrom(s => DateTime.SpecifyKind(s.RegistrationDateTime, DateTimeKind.Utc)))
                .ForMember(d => d.BirthDate, memberOptions => 
                {
                    memberOptions.MapFrom(s => s.BirthDate.HasValue ? DateTime.SpecifyKind(s.BirthDate.Value, DateTimeKind.Utc) : s.BirthDate);
                })
                .ForMember(d => d.Weight, memberOptions => memberOptions.MapFrom(s => s.Weight))
                .ForMember(d => d.Height, memberOptions => memberOptions.MapFrom(s => s.Height));
            CreateMap<ModelUser, EFUser>()
                .ForMember(d => d.Id, memberOptions => memberOptions.MapFrom(s => s.Id))
                .ForMember(d => d.Login, memberOptions => memberOptions.Condition(s => s.Login != null))
                .ForMember(d => d.RegistrationDateTime, memberOptions => memberOptions.MapFrom(s => s.RegistrationDateTime))
                .ForMember(d => d.BirthDate, memberOptions => memberOptions.MapFrom(s => s.BirthDate))
                .ForMember(d => d.Weight, memberOptions => memberOptions.MapFrom(s => s.Weight))
                .ForMember(d => d.Height, memberOptions => memberOptions.MapFrom(s => s.Height));
        }
    }
}
