﻿using AutoMapper;
using ModelActivityDetail = ActivityRecords.Data.Repository.Model.Entities.ActivityDetail;
using EFActivityDetail = ActivityRecords.Data.Repository.EFModels.ActivityDetail;

namespace ActivityRecords.Data.Repository.MapperProfiles
{
    public class ActivityDetailProfile : Profile
    {
        public ActivityDetailProfile()
        {
            CreateMap<ModelActivityDetail, EFActivityDetail>()
                .ForMember(d => d.ActivityId, opts => opts.MapFrom(s => s.ActivityId))
                .ForMember(d => d.Name, opts => opts.MapFrom(s => s.Name))
                .ForMember(d => d.Value, opts => opts.MapFrom(s => s.Value));
            CreateMap<EFActivityDetail,ModelActivityDetail>()
                .ForMember(d => d.ActivityId, opts => opts.MapFrom(s => s.ActivityId))
                .ForMember(d => d.Name, opts => opts.MapFrom(s => s.Name))
                .ForMember(d => d.Value, opts => opts.MapFrom(s => s.Value));
        }
    }
}
