﻿using AutoMapper;
using System.Linq;
using EFActivity = ActivityRecords.Data.Repository.EFModels.Activity;
using ModelActivity = ActivityRecords.Data.Repository.Model.Entities.Activity;
using ModelActivityDetail = ActivityRecords.Data.Repository.Model.Entities.ActivityDetail;
using EFActivityDetail = ActivityRecords.Data.Repository.EFModels.ActivityDetail;
using System;

namespace ActivityRecords.Data.Repository.MapperProfiles
{
    public class ActivityProfile : Profile
    {
        public ActivityProfile()
        {
            CreateMap<EFActivity, ModelActivity>()
                .ForMember(d => d.Id, opts => opts.MapFrom(s => s.Id))
                .ForMember(d => d.UserId, opts => opts.MapFrom(s => s.UserId))
                .ForMember(d => d.ActivityTypeId, opts => opts.MapFrom(s => s.ActivityTypeId))
                .ForMember(d => d.Date, opts => opts.MapFrom(s => DateTime.SpecifyKind(s.Date, DateTimeKind.Utc)))
                .ForMember(d => d.Details, opts => opts.MapFrom(s => s.ActivityDetails.Select(ad => new ModelActivityDetail
                {
                    ActivityId = ad.ActivityId,
                    Name = ad.Name,
                    Value = ad.Value
                })));
            CreateMap<ModelActivity, EFActivity>()
                .ForMember(d => d.Id, opts => opts.MapFrom(s => s.Id))
                .ForMember(d => d.UserId, opts => opts.MapFrom(s => s.UserId))
                .ForMember(d => d.ActivityTypeId, opts => opts.MapFrom(s => s.ActivityTypeId))
                .ForMember(d => d.Date, opts => opts.MapFrom(s => s.Date.ToUniversalTime()))
                .ForMember(d => d.ActivityDetails, opts => opts.MapFrom(s => s.Details.Select(ad => new EFActivityDetail
                {
                    ActivityId = ad.ActivityId,
                    Name = ad.Name,
                    Value = ad.Value
                })));
        }
    }
}
