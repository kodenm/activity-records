﻿using AutoMapper;
using System;

namespace ActivityRecords.Data.Repository.MapperProfiles
{
    public static class MapperExtension
    {
        public static TDestination MultipleMap<TDestination>(this IMapper mapper, 
            Action<IMappingOperationOptions> opts, 
            object main, 
            params object[] others)
        {
            var result = mapper.Map<TDestination>(main, opts);
            foreach (var objectToMap in others)
            {
                mapper.Map(objectToMap, result, opts);
            }
            return result;
        }
    }
}
