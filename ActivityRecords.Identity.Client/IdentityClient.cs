﻿using ActivityRecords.Client.Common;
using ActivityRecords.Identity.Api.Model.Entities;
using ActivityRecords.Identity.Api.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ActivityRecords.Identity.Client
{
    public class IdentityClient : ClientBase, IIdentityClient
    {
        public IdentityClient(Uri serverUri) : base(serverUri)
        {
        }

        public async Task<int> RegisterAsync(Credentials credentials)
        {
            var response = await PostAsyncInternal("signup", credentials);
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            var accessToken = JsonConvert.DeserializeAnonymousType(await response.Content.ReadAsStringAsync(), new { accessToken = "" }).accessToken;
            var id = int.Parse(new JwtSecurityTokenHandler().ReadJwtToken(accessToken).Claims.First(c => c.Type == "id").Value);
            return id;
        }
        public async Task DeleteAsync(int id)
        {
            var response = await _client.DeleteAsync($"{id}");
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task UpdateAsync(int id, Credentials credentials)
        {
            var response = await PutAsyncInternal($"update/{id}", credentials);
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
        }
    }
}
