﻿using System;

namespace ActivityRecords.Data.Api.Model.User
{
    public class CreationUserModel
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime? BirthDate { get; set; }
        public float? Weight { get; set; }
        public int? Height { get; set; }
    }
}
