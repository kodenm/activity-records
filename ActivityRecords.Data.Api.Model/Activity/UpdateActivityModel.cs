﻿using ActivityRecords.Data.Api.Model.ActivityDetail;
using System.Collections.Generic;

namespace ActivityRecords.Data.Api.Model.Activity
{
    public class UpdateActivityModel
    {
        public IEnumerable<ActivityDetailModel> Details { get; set; }
    }
}
