﻿namespace ActivityRecords.Data.Api.Model.ActivityDetail
{
    public class ActivityDetailModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
