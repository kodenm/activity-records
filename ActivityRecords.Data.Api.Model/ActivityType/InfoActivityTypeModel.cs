﻿using System.Collections.Generic;

namespace ActivityRecords.Data.Api.Model.ActivityType
{
    public class InfoActivityTypeModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public IEnumerable<string> DetailNames { get; set; }
    }
}
