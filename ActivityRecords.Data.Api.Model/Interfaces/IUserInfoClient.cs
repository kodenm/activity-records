﻿using System.Threading.Tasks;
using ActivityRecords.Data.Api.Model.User;

namespace ActivityRecords.Data.Api.Model.Interfaces
{
    public interface IUserInfoClient
    {
        Task<InfoUserModel> GetByIdAsync(int id);
        Task<InfoUserModel> CreateAsync(CreationUserModel creationUser);
        Task<InfoUserModel> UpdateAsync(int id, UpdateUserModel updateUser);
        Task DeleteAsync(int id);
    }
}