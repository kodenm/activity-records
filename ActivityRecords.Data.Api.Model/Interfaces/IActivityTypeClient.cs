﻿using ActivityRecords.Data.Api.Model.ActivityType;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ActivityRecords.Data.Api.Model.Interfaces
{
    public interface IActivityTypeClient
    {
        Task<InfoActivityTypeModel> GetById(int id);
        Task<IEnumerable<InfoActivityTypeModel>> GetByUserId(int userId);
        Task<InfoActivityTypeModel> Create(CreationActivityTypeModel creationActivityType);
        Task<InfoActivityTypeModel> Update(int id, UpdateActivityTypeModel updateActivityType);
        Task Delete(int id);
    }
}
