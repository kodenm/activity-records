using ActivityRecords.Gateway.Api.Service.Miscellaneous;
using ActivityRecords.Data.Api.Client;
using ActivityRecords.Data.Api.Model.Interfaces;
using ActivityRecords.Identity.Api.Model.Interfaces;
using ActivityRecords.Identity.Client;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;

namespace ActivityRecords.Gateway.Api.Service
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.Configure<AuthOptions>(Configuration.GetSection("Auth"));

            Uri storageServiceUri = new Uri(Configuration.GetConnectionString("DataServiceUri"));
            Uri authServiceUri = new Uri(Configuration.GetConnectionString("AuthServiceUri"));
            services.AddScoped<IUserInfoClient, UserInfoClient>(uic => new UserInfoClient(storageServiceUri));
            services.AddScoped<IIdentityClient, IdentityClient>(rc => new IdentityClient(authServiceUri));
            services.AddScoped<IActivityTypeClient, ActivityTypeClient>(sc => new ActivityTypeClient(storageServiceUri));
            services.AddScoped<IActivityClient, ActivityClient>(sc => new ActivityClient(storageServiceUri));
            services.AddHttpContextAccessor();

            services.AddCors(setupAction => setupAction.AddDefaultPolicy(setupAction => setupAction.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(configureOptions =>
            {
                var authOptions = Configuration.GetSection("Auth").Get<AuthOptions>();
                configureOptions.RequireHttpsMetadata = false;
                configureOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = authOptions.GetSymmetricsSecurityKey(),
                    ValidateIssuer = true,
                    ValidIssuer = authOptions.Issuer,
                    ValidateLifetime = true,
                    ValidAudience = authOptions.Audience
                };
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors();
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
