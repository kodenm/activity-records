﻿using ActivityRecords.Data.Api.Model.Activity;
using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Api.Model.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ActivityRecords.Gateway.Api.Service.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class ActivityController : Controller
    {
        private IActivityClient _activityClient;
        private IActivityTypeClient _activityTypeClient;

        public ActivityController(IActivityClient activityClient, IActivityTypeClient activityTypeClient)
        {
            _activityClient = activityClient;
            _activityTypeClient = activityTypeClient;
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreationActivityModel creationActivity)
        {
            try
            {
                InfoActivityTypeModel activityType = await _activityTypeClient.GetById(creationActivity.ActivityTypeId);
                if (!Enumerable.SequenceEqual(activityType.DetailNames, creationActivity.Details.Select(d => d.Name))) {
                    throw new ArgumentException("Request activity details don't correspond its activity type details names");
                }
                return new OkObjectResult(await _activityClient.Create(creationActivity));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                return new OkObjectResult(await _activityClient.GetById(id));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetByUserId(int userId)
        {
            try
            {
                return new OkObjectResult(await _activityClient.GetByUserId(userId));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, UpdateActivityModel updateActivity)
        {
            try
            {
                InfoActivityModel activity = await _activityClient.GetById(id);
                if (!Enumerable.SequenceEqual(activity.Details.Select(d => d.Name), updateActivity.Details.Select(d => d.Name)))
                {
                    throw new ArgumentException("Request activity details don't correspond its already written record");
                }
                return new OkObjectResult(await _activityClient.Update(id, updateActivity));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _activityClient.Delete(id);
                return Ok();
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
