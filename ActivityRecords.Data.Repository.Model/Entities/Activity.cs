﻿using System;
using System.Collections.Generic;

namespace ActivityRecords.Data.Repository.Model.Entities
{
    public class Activity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public int ActivityTypeId { get; set; }
        public IEnumerable<ActivityDetail> Details { get; set; }
    }
}
