﻿using ActivityRecords.Data.Repository.Model.Entities;

namespace ActivityRecords.Data.Repository.Model.Interfaces
{
    public interface IActivityFilter
    {
        public bool FitsCreteria(Activity activity);
    }
}
