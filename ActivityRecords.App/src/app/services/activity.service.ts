import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { environment } from '../environment';
import { Activity } from '../interfaces/activity';
import { AccountService } from './account.service';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(private http: HttpClient, private accountService: AccountService) { 
  }

  getByUserId(): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${environment.gatewayServiceUrl}/activity/user/${this.accountService.userId}`);
  }

  removeById(id: number): Observable<any> {
    return this.http.delete(`${environment.gatewayServiceUrl}/activity/${id}`);
  }

  create(activity: Activity): Observable<Activity> {
    return this.http.post<Activity>(`${environment.gatewayServiceUrl}/activity`, activity);
  }

  update(id: number, activity: Activity): Observable<Activity> {
    return this.http.put<Activity>(`${environment.gatewayServiceUrl}/activity/${id}`, activity);
  }
}
