import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidators } from 'src/app/custom-validators';
import { User } from 'src/app/interfaces/user';
import { AccountService } from 'src/app/services/account.service';

enum AuthType {
  Login,
  Registration
}

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.css', '../../../shared.styles.css']
})
export class AuthFormComponent implements OnInit {
  authType = AuthType;
  formType?: AuthType = 0;
  formGroup: FormGroup = new FormGroup({
    login: new FormControl(undefined, [Validators.required, CustomValidators.lettersAndDigits()]),
    password: new FormControl(undefined, [Validators.required, CustomValidators.lettersAndDigits()])
  })
  constructor(private accountService: AccountService, private router: Router, private activatedRoute: ActivatedRoute) { }


  ngOnInit(): void {
    if (this.accountService.userId !== -1) {
      this.router.navigate(['activities']);
    }
    this.formType = this.activatedRoute.snapshot.url.pop()?.toString() == 'login' ? AuthType.Login : AuthType.Registration;
    if (this.formType === this.authType.Registration) {
      this.formGroup.addControl('birthDate', new FormControl(undefined));
      this.formGroup.addControl('weight', new FormControl(undefined, CustomValidators.isPositiveNumber(true)));
      this.formGroup.addControl('height', new FormControl(undefined, CustomValidators.isPositiveNumber(true)));
    }
  }

  onSubmit(): void {
    if (this.formType === AuthType.Login) {
      this.accountService.login(this.formGroup.value as User);
    }
    else if (this.formType === AuthType.Registration){
      this.accountService.signup(this.formGroup.value as User);
    }
  }
}
