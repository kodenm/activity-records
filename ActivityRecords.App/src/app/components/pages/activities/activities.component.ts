import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Activity } from 'src/app/interfaces/activity';
import { ActivityType } from 'src/app/interfaces/activity-type';
import { ActivityTypeService } from 'src/app/services/activity-type.service';
import { ActivityService } from 'src/app/services/activity.service';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css', '../../../shared.styles.css']
})
export class ActivitiesComponent implements OnInit {
  activities?: Activity[];
  activityTypes?: ActivityType[];
  previewActivities?: Activity[];
  filterActivityTypeId: string = "0";
  filterOrderType = 'asc';
  constructor(private activityService: ActivityService, private activityTypeService: ActivityTypeService, private router: Router, private route: ActivatedRoute, private location: Location) { 

  }

  ngOnInit(): void {
    this.filterActivityTypeId = this.route.snapshot.paramMap.get('typeId') ?? "0";
    this.activityTypeService.getActivitiesByUserId().subscribe(activityTypes => {
      this.activityTypes = activityTypes;
    }, () => {}, () => {
      if (this.activityTypes?.filter(at => at.id === Number(this.filterActivityTypeId)).length === 0) {
        this.router.navigate(['activities']);
      }
    });
    this.activityService.getByUserId().subscribe((activities: Activity[]) => {
      this.activities = activities;
      this.previewActivities = [...(this.filterActivityTypeId !== "0"
        ? this.activities.filter(a => a.activityTypeId === Number(this.filterActivityTypeId)) : this.activities)];
    });
  }

  removeActivity(activity: Activity): void {
    this.activityService.removeById(activity.id ?? -1).subscribe(() => {}, () => {}, () => {
      let index: number = this.activities?.indexOf(activity) ?? -1;
      this.activities?.splice(index, 1);
      this.previewActivities = this.filterActivityTypeId !== "0" ? this.activities?.filter(a => a.activityTypeId === Number(this.filterActivityTypeId)) : this.activities;
    });
  }

  onActivityTypeChange(type: any): void {
    if (type.value === "0") {
      this.previewActivities = this.activities;
      this.location.go("activities");
    }
    else {
      this.location.go(`activities/type/${type.value}`);
      this.previewActivities = this.activities?.filter(a => a.activityTypeId === Number(type.value));
    }
  }
}
