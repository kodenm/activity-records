import { Component, Input, OnInit, Output } from '@angular/core';
import { ActivityType } from 'src/app/interfaces/activity-type';
import { EventEmitter } from '@angular/core';
import { ActivityTypeService } from 'src/app/services/activity-type.service';
import { FormArray, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { CustomValidators } from 'src/app/custom-validators';

@Component({
  selector: 'app-activity-type-card',
  templateUrl: './activity-type-card.component.html',
  styleUrls: ['./activity-type-card.component.css', "../../../shared.styles.css"]
})
export class ActivityTypeCardComponent implements OnInit {
  @Input() activityType: ActivityType = {};
  @Output() onDeleted = new EventEmitter();
  isEditMode: boolean = false;
  errorText: string = '';
  formGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, CustomValidators.lettersAndDigitsWithSpace()]),
    detailNames: new FormArray([], CustomValidators.formArrayIsUnique())
  });
  constructor(private activityTypeService: ActivityTypeService, private router: Router) { }

  ngOnInit(): void {
  }

  toggleEditMode(): void {
    this.isEditMode = !this.isEditMode;
    this.formGroup.controls['name'].setValue(this.activityType.name);
    let formArray: FormArray = this.getDetails();
    formArray.clear();
    this.activityType.detailNames?.forEach(d => this.getDetails().push(new FormControl(d, [Validators.required, CustomValidators.lettersAndDigitsWithSpace()])));
  }

  update(): void {
    this.formGroup.disable();
    this.activityTypeService.update(this.activityType.id ?? -1, this.formGroup.value).subscribe((activityType: any) => {
      this.formGroup.enable();
      this.activityType = activityType as ActivityType;
      
    },
    (error: HttpErrorResponse) => {
      this.errorText = error.error;
      this.formGroup.enable();
    },
    () => {
      this.toggleEditMode();
    });
  }

  remove(): void {
    this.onDeleted.emit(this.activityType);
  }

  addNewDetail(): void {
    this.getDetails().push(new FormControl('', [Validators.required, CustomValidators.lettersAndDigitsWithSpace()]));
  }

  viewActivities(): void {
    this.router.navigate(['activities', 'type', this.activityType.id]);
  }

  getDetails(): FormArray {
    return this.formGroup.controls['detailNames'] as FormArray;
  }

  removeDetail(index: number): void {
    this.getDetails().removeAt(index);
  }
}
