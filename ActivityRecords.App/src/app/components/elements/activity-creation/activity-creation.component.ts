import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Activity } from 'src/app/interfaces/activity';
import { ActivityType } from 'src/app/interfaces/activity-type';
import { AccountService } from 'src/app/services/account.service';
import { ActivityTypeService } from 'src/app/services/activity-type.service';
import { ActivityService } from 'src/app/services/activity.service';

@Component({
  selector: 'app-activity-creation',
  templateUrl: './activity-creation.component.html',
  styleUrls: ['./activity-creation.component.css', '../../../shared.styles.css']
})
export class ActivityCreationComponent implements OnInit {
  errorText: string = "";
  @Output() onCreated = new EventEmitter();
  formGroup: FormGroup = new FormGroup({
    activityTypeId: new FormControl(undefined, [Validators.required]),
    userId: new FormControl(undefined, [Validators.required]),
    dateTime: new FormControl(undefined, Validators.required),
    details: new FormArray([])
  });
  activityTypes: ActivityType[] = [];
  constructor(private accountService: AccountService, private activityService: ActivityService, private activityTypeService: ActivityTypeService, private router: Router) { }
  
  ngOnInit(): void {
    this.formGroup.controls['userId'].setValue(this.accountService.userId);
    this.activityTypeService.getActivitiesByUserId().subscribe(activityTypes => this.activityTypes = activityTypes);
    (this.formGroup.controls['details'] as FormArray).disable();
  }

  onSubmit(): void {
    this.formGroup.controls['dateTime'].setValue(new Date(this.formGroup.controls['dateTime'].value).toISOString());
    this.formGroup.controls['activityTypeId'].setValue(Number(this.formGroup.controls['activityTypeId'].value));
    this.activityService.create(this.formGroup.getRawValue() as Activity).subscribe(
      () => this.router.navigate(['activities']), 
      (error: HttpErrorResponse) => alert(error.error)
    );
  }

  onTypeChange(event: any): void {
    let formArray: FormArray = this.formGroup.controls['details'] as FormArray;
    formArray.clear();
    this.formGroup.controls['dateTime'].reset();
    this.activityTypes.find(at => (at.id ?? 0) === Number(event.value))
      ?.detailNames?.forEach(d => 
        formArray.controls.push(new FormGroup({ name: new FormControl(d), value: new FormControl(undefined) })));
  }

  getDetailForms(): FormGroup[] {
    return (this.formGroup.controls['details'] as FormArray).controls.map(c => c as FormGroup);
  }
}
